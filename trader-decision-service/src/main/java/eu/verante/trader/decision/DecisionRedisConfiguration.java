package eu.verante.trader.decision;

import eu.verante.trader.decision.maker.indicator.IndicatorHashKey;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

import java.util.Map;

@Configuration
public class DecisionRedisConfiguration {

    @Bean
    public RedisTemplate<String, IndicatorHashKey> redisIndicatorValueTemplate(RedisConnectionFactory jedisConnectionFactory) {
        RedisTemplate<String, IndicatorHashKey> template = new RedisTemplate<>();
        template.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(IndicatorHashKey.class));
        template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Map.class));
        template.setConnectionFactory(jedisConnectionFactory);
        return template;
    }
}
