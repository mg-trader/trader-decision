package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.decision.maker.DecisionMaker;
import eu.verante.trader.decision.maker.indicator.configuration.DecisionMakerConfiguration;
import eu.verante.trader.indicator.domain.IndicatorValueType;
import eu.verante.trader.indicator.domain.value.IndicatorValue;

import java.util.Map;

public abstract class IndicatorValueDecisionMaker<V extends IndicatorValue, DMC extends DecisionMakerConfiguration>
        implements DecisionMaker<DMC> {

    private final IndicatorValueProvider<V> valueProvider;

    private final ObjectMapper mapper;

    protected IndicatorValueDecisionMaker(IndicatorValueProvider<V> valueProvider, ObjectMapper mapper) {
        this.valueProvider = valueProvider;
        this.mapper = mapper;
    }

    protected IndicatorValueProvider<V> getValueProvider() {
        return valueProvider;
    }

    protected String createContext(IndicatorValueType valueType, IndicatorValue value) {
        try {
            return mapper.writeValueAsString(Map.of(valueType, value));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
