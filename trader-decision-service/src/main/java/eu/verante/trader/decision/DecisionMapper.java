package eu.verante.trader.decision;

import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.domain.DecisionPredicateDefinition;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DecisionMapper {

    DecisionPredicateDefinition toDefinition(DecisionPredicate predicate);

    DecisionPredicate toPredicate(DecisionPredicateDefinition predicate);
}
