package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.decision.maker.indicator.configuration.EmptyDecisionMakerConfiguration;
import eu.verante.trader.indicator.domain.value.CompareIndicatorValue;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CompareDecisionMakerFactory
        extends AbstractIndicatorValueDecisionMakerFactory<
        CompareIndicatorValue, EmptyDecisionMakerConfiguration, CompareDecisionMaker> {

    protected CompareDecisionMakerFactory() {
        super("COMPARE_INDICATORS");
    }

    @Override
    public CompareDecisionMaker create(IndicatorValueProvider<CompareIndicatorValue> provider, Map<String, Object> configuration) {
        return new CompareDecisionMaker(provider, getObjectMapper());
    }
}
