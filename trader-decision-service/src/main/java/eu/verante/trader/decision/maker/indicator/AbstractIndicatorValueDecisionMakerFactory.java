package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.decision.maker.IndicatorValueDecisionMakerFactory;
import eu.verante.trader.decision.maker.indicator.configuration.DecisionMakerConfiguration;
import eu.verante.trader.indicator.domain.IndicatorValueType;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public abstract class AbstractIndicatorValueDecisionMakerFactory<
        V extends IndicatorValue,
        DMC extends DecisionMakerConfiguration,
        DM extends IndicatorValueDecisionMaker<V, DMC>>
        implements IndicatorValueDecisionMakerFactory<V, DMC, DM> {

    private final String type;

    @Autowired
    private ObjectMapper objectMapper;


    protected AbstractIndicatorValueDecisionMakerFactory(String indicatorType) {
        this.type = indicatorType;
    }

    @Override
    public String getHandledType() {
        return type;
    }

    protected ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    protected DMC toConfiguration(Map<String, Object> map, Class<DMC> configType) {
//        Map<String, Object> config = (Map<String, Object>) map.get("configuration");
//        config.get("configurationType");
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(map), configType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
