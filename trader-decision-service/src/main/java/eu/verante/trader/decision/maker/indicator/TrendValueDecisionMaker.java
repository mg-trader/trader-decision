package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.domain.DecisionType;
import eu.verante.trader.decision.maker.indicator.configuration.EmptyDecisionMakerConfiguration;
import eu.verante.trader.indicator.domain.IndicatorValueType;
import eu.verante.trader.indicator.domain.value.TrendIndicatorValue;

import java.time.ZonedDateTime;

public class TrendValueDecisionMaker
        extends IndicatorValueDecisionMaker<TrendIndicatorValue, EmptyDecisionMakerConfiguration> {

    TrendValueDecisionMaker(IndicatorValueProvider<TrendIndicatorValue> provider, ObjectMapper mapper) {
        super(provider, mapper);
    }

    @Override
    public void initialize(EmptyDecisionMakerConfiguration config) {}

    @Override
    public DecisionPredicate make(ZonedDateTime date) {
        TrendIndicatorValue indicatorValue = getValueProvider().getValue(date);
        String context = createContext(IndicatorValueType.TREND, indicatorValue);
        TrendIndicatorValue previousValue = getValueProvider().getValue(date, -1);
        if (previousValue != null && indicatorValue.getTrendType() == previousValue.getTrendType()) {
            return new DecisionPredicate(DecisionType.HOLD, context);
        }

        return switch (indicatorValue.getTrendType()) {
            case RISING -> new DecisionPredicate(DecisionType.BUY, context);
            case FALLING -> new DecisionPredicate(DecisionType.SELL, context);
            default -> new DecisionPredicate(DecisionType.HOLD, context);
        };
    }
}
