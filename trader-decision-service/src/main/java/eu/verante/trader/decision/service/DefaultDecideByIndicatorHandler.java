package eu.verante.trader.decision.service;

import eu.verante.trader.decision.DecisionMapper;
import eu.verante.trader.decision.client.IndicatorClient;
import eu.verante.trader.decision.client.IndicatorTypeClient;
import eu.verante.trader.decision.domain.*;
import eu.verante.trader.decision.maker.IndicatorValueDecisionMakerFactory;
import eu.verante.trader.decision.maker.indicator.IndicatorValueDecisionMaker;
import eu.verante.trader.decision.maker.indicator.IndicatorValueProvider;
import eu.verante.trader.decision.maker.indicator.IndicatorValueProviderFactory;
import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.util.asyncprocess.AbstractAsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class DefaultDecideByIndicatorHandler extends AbstractAsyncProcessHandler<DecideByIndicatorRequest> implements DecideByIndicatorHandler {

    @Autowired
    private IndicatorClient indicatorClient;

    @Autowired
    private IndicatorTypeClient indicatorTypeClient;

    @Autowired
    private IndicatorValueProviderFactory providerFactory;

    @Autowired
    private DecisionMapper decisionMapper;

    @Autowired
    private DecisionService decisionService;


    private Map<String, IndicatorValueDecisionMakerFactory> decisionMakers;

    @Autowired
    public void setDmFactories(Collection<IndicatorValueDecisionMakerFactory> dmFactories) {
        this.decisionMakers = dmFactories.stream()
                .collect(Collectors.toMap(
                        IndicatorValueDecisionMakerFactory::getHandledType,
                        Function.identity()
                ));
    }

    @Override
    protected void doProcess(DecideByIndicatorRequest request, AsyncProcess process) {
        Indicator indicator = indicatorClient.getIndicatorByName(request.getIndicator());
        IndicatorType type = indicatorTypeClient.getIndicatorTypeByName(indicator.getType());
        assert decisionMakers.containsKey(type.getValueType());

        IndicatorValueProvider<?> provider = providerFactory.create(indicator, request.getSymbol(), request.getPeriod(),
                Instant.ofEpochMilli(request.getFrom()).atZone(ZoneId.systemDefault()),
                Instant.ofEpochMilli(request.getTo()).atZone(ZoneId.systemDefault()));
        IndicatorValueDecisionMaker decisionMaker = decisionMakers.get(type.getType()).create(provider, request.getConfiguration());

        DecisionDocument decisionDocument = getOrCreateDocument(request, indicator);
        SortedMap<Long, DecisionPredicateDefinition> decisions = new TreeMap<>();

        for (ZonedDateTime providedDate : provider) {
            DecisionPredicate decision = decisionMaker.make(providedDate);
            decisions.put(providedDate.toInstant().toEpochMilli(), decisionMapper.toDefinition(decision));
        }
        decisionDocument.setDecisions(decisions);

        decisionService.save(decisionDocument);
    }

    private DecisionDocument getOrCreateDocument(DecideByIndicatorRequest request, Indicator indicator) {
        Optional<DecisionDocument> foundDocument = decisionService.findDocument(request);

        return foundDocument.orElseGet(() -> {
            DecisionDocument newDocument = new DecisionDocument();
            newDocument.asNewDocument();

            newDocument.setDecisionGroupTypeDefinition(DecisionGroupTypeDefinition.INDICATOR);
            IndicatorDecisionMakerInfo decisionMakerInfo = new IndicatorDecisionMakerInfo();
            decisionMakerInfo.setIndicatorName(indicator.getName());
            decisionMakerInfo.setIndicatorType(indicator.getType());
            decisionMakerInfo.setConfiguration(request.getConfiguration());
            newDocument.setDecisionMakerInfo(decisionMakerInfo);
            newDocument.setSymbol(request.getSymbol());
            newDocument.setPeriod(newDocument.getPeriod());
            return newDocument;
        });
    }
}
