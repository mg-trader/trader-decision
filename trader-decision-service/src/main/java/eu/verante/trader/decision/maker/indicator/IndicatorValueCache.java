package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.stock.candle.Period;

import java.time.ZonedDateTime;
import java.util.SortedMap;

public interface IndicatorValueCache {

    <V extends IndicatorValue> void store(Indicator indicator, String symbol, Period period,
                                    ZonedDateTime from, ZonedDateTime to,
                                    SortedMap<ZonedDateTime, PreloadedValue<V>> values);

    <V extends IndicatorValue> SortedMap<ZonedDateTime, PreloadedValue<V>> load(
            Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to, IndicatorType type);

    void clean();
}

