package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.decision.maker.indicator.configuration.EmptyDecisionMakerConfiguration;
import eu.verante.trader.indicator.domain.value.TrendIndicatorValue;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TrendValueDecisionMakerFactory
        extends AbstractIndicatorValueDecisionMakerFactory<
        TrendIndicatorValue, EmptyDecisionMakerConfiguration, TrendValueDecisionMaker> {

    TrendValueDecisionMakerFactory() {
        super("TREND");
    }

    @Override
    public TrendValueDecisionMaker create(IndicatorValueProvider<TrendIndicatorValue> provider, Map<String, Object> configuration) {
        return new TrendValueDecisionMaker(provider, getObjectMapper());
    }
}
