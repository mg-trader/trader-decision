package eu.verante.trader.decision.api;

import eu.verante.trader.decision.domain.CalculateDecisionByIndicatorFitnessRequest;
import eu.verante.trader.decision.domain.CalculateSyntheticFitnessRequest;
import eu.verante.trader.decision.domain.DecisionFitness;
import eu.verante.trader.decision.domain.FitnessResource;
import eu.verante.trader.decision.service.FitnessCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fitness")
public class FitnessController implements FitnessResource {

    @Autowired
    private FitnessCalculationService fitnessCalculationService;

    @Override
    public DecisionFitness calculateFitnessByRequest(CalculateDecisionByIndicatorFitnessRequest request) {
        return fitnessCalculationService.calculateFitness(request);
    }

    @Override
    public DecisionFitness calculateFitness(CalculateSyntheticFitnessRequest request) {
        return fitnessCalculationService.calculateFitness(request);
    }
}
