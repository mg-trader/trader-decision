package eu.verante.trader.decision.maker.indicator.configuration;

import lombok.Data;

@Data
public class SimpleOscillatorConfiguration implements DecisionMakerConfiguration {

    private double decisionThreshold;
}
