package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;

public class MomentumDecisionMaker
        extends AbstractOscillatorDecisionMaker {

    protected MomentumDecisionMaker(IndicatorValueProvider<SimpleIndicatorValue<Double>> valueProvider, ObjectMapper mapper) {
        super(valueProvider, mapper, 100.0d);
    }
}
