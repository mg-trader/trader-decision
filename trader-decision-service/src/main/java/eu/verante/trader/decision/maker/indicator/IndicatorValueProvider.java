package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.indicator.domain.value.IndicatorValue;

import java.time.ZonedDateTime;

public interface IndicatorValueProvider<V extends IndicatorValue>
        extends Iterable<ZonedDateTime>{
    V getValue(ZonedDateTime date);

    V getValue(ZonedDateTime date, int periodOffset);
}
