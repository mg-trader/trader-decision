package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.decision.client.IndicatorCalculationClient;
import eu.verante.trader.decision.client.IndicatorTypeClient;
import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.indicator.domain.request.CalculationRequest;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import eu.verante.trader.util.asyncprocess.AsyncProcessAwaitFactory;
import eu.verante.trader.util.asyncprocess.AsyncProcessStatusChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@Component
@Scope("prototype")
public class PreloadingIndicatorValueProvider<V extends IndicatorValue> implements IndicatorValueProvider<V> {

    @Autowired
    private IndicatorCalculationClient calculationClient;

    @Autowired
    private IndicatorTypeClient typeClient;

    @Autowired
    private AsyncProcessAwaitFactory awaitFactory;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private IndicatorValueCache valueCache;
    private SortedMap<ZonedDateTime, PreloadedValue<V>> preloadedValues;
    private Period loadedPeriod;

    void preloadValues(Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        IndicatorType type = typeClient.getIndicatorTypeByName(indicator.getType());

        if (!preloadFromCache(indicator, symbol, period, from, to, type)) {
            preloadFromService(indicator, symbol, period, from, to, type);
            storeToCache(indicator, symbol, period, from, to);
        }

    }

    private void storeToCache(Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        valueCache.store(indicator, symbol, period, from, to, preloadedValues);
    }

    private void preloadFromService(Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to, IndicatorType type) {
        CalculationRequest request = CalculationRequest.builder()
                .indicator(indicator.getName())
                .symbol(symbol)
                .period(period)
                .from(from)
                .to(to)
                .build();
        AsyncProcess process = calculationClient.calculate(request);
        try {
            awaitFactory.createFor(createStatusChecker(process.getProcessId())).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        this.preloadedValues =  new TreeMap<>();

        Optional<PreloadedValue<V>> last = Optional.empty();
        for (Map.Entry<Long, JsonNode> entry : calculationClient.getCalculationResultsRaw(request).entrySet()) {
            ZonedDateTime key = Instant.ofEpochMilli(entry.getKey()).atZone(ZoneId.systemDefault());

            PreloadedValue<V> value = new PreloadedValue<>();
            value.setValue((V)fromJson(entry.getValue(), type.getValueType().getValueClass()));
            value.setPrevious(last);
            last.ifPresent( l-> l.setNext(Optional.of(value)));

            preloadedValues.put(key, value);
            last = Optional.of(value);
        }

    }

    private boolean preloadFromCache(Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to, IndicatorType type) {
        this.preloadedValues = valueCache.load(indicator, symbol, period, from, to, type);
        return preloadedValues != null;
    }

    @Override
    public V getValue(ZonedDateTime date) {
        return preloadedValues.get(date).getValue();
    }

    @Override
    public V getValue(ZonedDateTime date, int periodOffset) {
        if (periodOffset == 0) {
            return getValue(date);
        }
        Optional<PreloadedValue<V>> currentValue = Optional.of(preloadedValues.get(date));
        int currentOffset = periodOffset;
        if (currentOffset >= 0) {
            while (currentOffset > 0 && currentValue.isPresent()) {
                currentValue = currentValue.get().getNext();
                currentOffset--;
            }
        } else {
            while (currentOffset < 0 && currentValue.isPresent()) {
                currentValue = currentValue.get().getPrevious();
                currentOffset++;
            }
        }
        return currentValue.map(cv -> cv.getValue()).orElse(null);
    }

    @Override
    public Iterator<ZonedDateTime> iterator() {
        return preloadedValues.keySet().iterator();
    }

    private AsyncProcessStatusChecker createStatusChecker(UUID processId) {
        return () -> calculationClient.getProcessStatus(processId);
    }

    private <N extends IndicatorValue> N fromJson(TreeNode value, Class<N> expectedType) {
        try {
            return objectMapper.treeToValue(value, expectedType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
