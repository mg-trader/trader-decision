package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.domain.DecisionType;
import eu.verante.trader.decision.maker.indicator.configuration.SimpleOscillatorConfiguration;
import eu.verante.trader.indicator.domain.IndicatorValueType;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;

import java.time.ZonedDateTime;

public abstract class AbstractOscillatorDecisionMaker
        extends IndicatorValueDecisionMaker<SimpleIndicatorValue<Double>, SimpleOscillatorConfiguration>{

    private final double middleValue;

    private double decisionThreshold;
    protected AbstractOscillatorDecisionMaker(IndicatorValueProvider<SimpleIndicatorValue<Double>> valueProvider, ObjectMapper mapper, double middleValue) {
        super(valueProvider, mapper);
        this.middleValue = middleValue;
    }

    @Override
    public void initialize(SimpleOscillatorConfiguration config) {
        this.decisionThreshold = config.getDecisionThreshold();
    }

    @Override
    public DecisionPredicate make(ZonedDateTime date) {
        SimpleIndicatorValue<Double> indicatorValue = getValueProvider().getValue(date);
        String context = createContext(IndicatorValueType.SIMPLE, indicatorValue);

        if (indicatorValue != null && indicatorValue.getValue() != null) {
            if (indicatorValue.getValue() >= (middleValue + decisionThreshold)) {
                return new DecisionPredicate(DecisionType.SELL, context);
            }
            if (indicatorValue.getValue() <= (middleValue - decisionThreshold)) {
                return new DecisionPredicate(DecisionType.BUY, context);
            }
        }
        return new DecisionPredicate(DecisionType.HOLD, context);
    }
}
