package eu.verante.trader.decision.service;

import eu.verante.trader.decision.domain.*;
import eu.verante.trader.decision.maker.indicator.IndicatorValueProviderFactory;
import eu.verante.trader.stock.candle.TradingCandle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

public class FitnessCalculator {

    private final Log logger = LogFactory.getLog(IndicatorValueProviderFactory.class);

    private final EnumSet<DecisionType> acceptedDecisionTypes;

    private double currentValue = 100.0;
    private List<Position> closedPositions = new ArrayList<>();

    private Position currentPosition;

    public FitnessCalculator(Collection<DecisionType> acceptedDecisionTypes) {
        if (!CollectionUtils.isEmpty(acceptedDecisionTypes)) {
            this.acceptedDecisionTypes = EnumSet.copyOf(acceptedDecisionTypes);
        } else {
            this.acceptedDecisionTypes = EnumSet.allOf(DecisionType.class);
        }
    }

    public void decide(DecisionPredicate predicate, TradingCandle candle) {
        if (predicate.getType() == DecisionType.HOLD) {
            return;
        }
        if (currentPosition != null) {
            if (currentPosition.getPositionType() == predicate.getType()) {
                logger.debug("Cannot create same position type twice! " + candle.time());
            } else {
                closeCurrentPosition(predicate, candle);
            }
        }
        if (currentPosition == null && acceptedDecisionTypes.contains(predicate.getType())) {
            setUpNewPosition(predicate, candle);
        }
    }

    public DecisionFitness getFitness() {
        DecisionFitness result = new DecisionFitness();
        result.setPositions(this.closedPositions);
        result.setCurrentPosition(this.currentPosition);

        List<Position> allPositions = new ArrayList<>(closedPositions);
        if (currentPosition != null) {
            allPositions.add(currentPosition);
        }

        result.setTotalStats(getStats(allPositions, Optional.empty()));
        result.setBuyStats(getStats(allPositions, Optional.of(DecisionType.BUY)));
        result.setSellStats(getStats(allPositions, Optional.of(DecisionType.SELL)));

        return result;
    }

    private DecisionStats getStats(List<Position> positions, Optional<DecisionType> positionType) {

        List<Position> filteredPositions = positions.stream()
                .filter(p -> positionType.isEmpty() || p.getPositionType() == positionType.get())
                .collect(Collectors.toList());

        DecisionStats stats = new DecisionStats();
        stats.setProfit(filteredPositions.stream()
                .mapToDouble(p -> p.getProfit())
                .sum());
        stats.setProfitPercent(stats.getProfit()*100.0/100.0);
        stats.setPositions(filteredPositions.size());
        stats.setProfitablePositions(filteredPositions.stream()
                .filter(this::isProfitable)
                .count());
        stats.setProfitablePositionsPercent(stats.getProfitablePositions()*100.0/stats.getPositions());
        return stats;
    }

    private boolean isProfitable(Position position) {
        return position!=null
                && position.getProfit() > 0.0;
    }

    private void setUpNewPosition(DecisionPredicate predicate, TradingCandle candle) {
        Position newPosition = new Position();
        newPosition.setPositionType(predicate.getType());

        Transaction open = new Transaction();
        open.setType(TransactionType.OPEN);
        open.setPrice(candle.close().doubleValue());
        open.setTime(candle.time());
        open.setVolume(currentValue/open.getPrice());
        open.setContext(predicate.getContext());
        newPosition.setOpen(open);

        currentPosition = newPosition;
    }

    public void markLastPositionAsClosed(TradingCandle candle) {
        Transaction close = new Transaction();
        close.setTime(candle.time());
        close.setType(TransactionType.CLOSE);
        close.setPrice(candle.close().doubleValue());
        close.setVolume(currentPosition.getOpen().getVolume());

        currentPosition.setClose(close);

        double buyOrSellSign = currentPosition.getPositionType() == DecisionType.BUY ? 1.0 : -1.0;

        double profit = (close.getValue() - currentPosition.getOpen().getValue()) * buyOrSellSign;
        currentPosition.setProfit(profit);
        currentPosition.setProfitPercent(profit*100.0/currentPosition.getOpen().getValue());

        currentValue = (100.0+currentPosition.getProfitPercent()) * currentValue/100.0;
    }

    private void closeCurrentPosition(DecisionPredicate predicate, TradingCandle candle) {
        markLastPositionAsClosed(candle);

        currentPosition.getClose().setContext(predicate.getContext());
        closedPositions.add(currentPosition);
        currentPosition = null;
    }


}
