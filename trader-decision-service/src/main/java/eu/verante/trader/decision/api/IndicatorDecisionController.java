package eu.verante.trader.decision.api;

import eu.verante.trader.decision.domain.DecideByIndicatorRequest;
import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.domain.IndicatorDecisionResource;
import eu.verante.trader.decision.service.DecisionService;
import eu.verante.trader.decision.service.IndicatorDecisionService;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/decision")
public class IndicatorDecisionController implements IndicatorDecisionResource {

    @Autowired
    private IndicatorDecisionService indicatorDecisionService;

    @Autowired
    private DecisionService decisionService;

    //post
    @Override
    public AsyncProcess decide(DecideByIndicatorRequest request) {
        return indicatorDecisionService.decide(request);
    }

    @Override
    public AsyncProcess getProcessStatus(UUID processId) {
        return indicatorDecisionService.getProcessStatus(processId);
    }

    @Override
    public Map<Long, DecisionPredicate> findByRequest(DecideByIndicatorRequest request) {
        return decisionService.findByRequest(request);
    }

    @Override
    public void cleanupAllDecisions() {
        decisionService.cleanup();
    }
}
