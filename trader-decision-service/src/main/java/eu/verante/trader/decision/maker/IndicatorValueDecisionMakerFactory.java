package eu.verante.trader.decision.maker;

import eu.verante.trader.decision.maker.indicator.IndicatorValueDecisionMaker;
import eu.verante.trader.decision.maker.indicator.IndicatorValueProvider;
import eu.verante.trader.decision.maker.indicator.configuration.DecisionMakerConfiguration;
import eu.verante.trader.indicator.domain.value.IndicatorValue;

import java.util.Map;

public interface IndicatorValueDecisionMakerFactory<
        V extends IndicatorValue,
        DMC extends DecisionMakerConfiguration,
        DM extends IndicatorValueDecisionMaker<V, DMC>> {

    String getHandledType();
    DM create(IndicatorValueProvider<V> provider, Map<String, Object> configuration);
}
