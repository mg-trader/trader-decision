package eu.verante.trader.decision.service;

import eu.verante.trader.decision.DecisionMapper;
import eu.verante.trader.decision.domain.*;
import eu.verante.trader.decision.maker.indicator.IndicatorValueCache;
import eu.verante.trader.decision.repository.DecisionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class DecisionService {

    @Autowired
    private DecisionRepository decisionRepository;

    @Autowired
    private IndicatorValueCache indicatorValueCache;

    @Autowired
    private DecisionMapper decisionMapper;

    Optional<DecisionDocument> findDocument(DecideByIndicatorRequest request) {
        DecisionDocument example = new DecisionDocument();
        example.setDecisionGroupTypeDefinition(DecisionGroupTypeDefinition.INDICATOR);
        IndicatorDecisionMakerInfo decisionMakerInfo = new IndicatorDecisionMakerInfo();
        decisionMakerInfo.setIndicatorName(request.getIndicator());
        decisionMakerInfo.setConfiguration(request.getConfiguration());
        example.setDecisionMakerInfo(decisionMakerInfo);
        example.setSymbol(request.getSymbol());
        example.setPeriod(example.getPeriod());

        return decisionRepository.findOne(Example.of(example));
    }

    public Map<Long, DecisionPredicate> findByRequest(DecideByIndicatorRequest request) {
        Optional<DecisionDocument> document = findDocument(request);
        return document
                .map(d -> d.getDecisions().entrySet())
                .map(entries -> entries.stream()
                        .filter(entry -> filterByDate(entry.getKey(), request))
                        .collect(Collectors.toMap(
                                entry -> entry.getKey(),
                                entry -> decisionMapper.toPredicate(entry.getValue())
                        )))
                .orElse(Map.of());
    }

    public void save(DecisionDocument document) {
        decisionRepository.save(document);
    }

    private boolean filterByDate(Long keyValue, DecideByIndicatorRequest request) {
        if (request.getFrom() != null && request.getFrom() > keyValue) {
            return false;
        }
        if (request.getTo() != null && request.getTo() < keyValue) {
            return false;
        }
        return true;
    }

    public void cleanup() {
        decisionRepository.deleteAll();
        indicatorValueCache.clean();
    }
}
