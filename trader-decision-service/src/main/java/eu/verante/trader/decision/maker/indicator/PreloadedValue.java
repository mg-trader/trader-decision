package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.indicator.domain.value.IndicatorValue;
import lombok.Data;

import java.util.Optional;

@Data
class PreloadedValue<V extends IndicatorValue> {
    private V value;
    private Optional<PreloadedValue<V>> previous = Optional.empty();
    private Optional<PreloadedValue<V>> next = Optional.empty();

}
