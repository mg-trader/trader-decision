package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.decision.maker.indicator.configuration.SimpleOscillatorConfiguration;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MomentumDecisionMakerFactory
        extends AbstractIndicatorValueDecisionMakerFactory
        <SimpleIndicatorValue<Double>, SimpleOscillatorConfiguration, MomentumDecisionMaker>{
    protected MomentumDecisionMakerFactory() {
        super("MOMENTUM");
    }

    @Override
    public MomentumDecisionMaker create(IndicatorValueProvider<SimpleIndicatorValue<Double>> provider, Map<String, Object> configuration) {
        MomentumDecisionMaker maker = new MomentumDecisionMaker(provider, getObjectMapper());
        maker.initialize(toConfiguration(configuration, SimpleOscillatorConfiguration.class));
        return maker;
    }
}
