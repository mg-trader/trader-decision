package eu.verante.trader.decision.service;

import eu.verante.trader.decision.domain.*;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import eu.verante.trader.util.asyncprocess.AsyncProcessAwaitFactory;
import eu.verante.trader.util.asyncprocess.AsyncProcessStatusChecker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

@Component
public class FitnessCalculationService {
    private final Log logger = LogFactory.getLog(FitnessCalculationService.class);

    @Autowired
    private DecisionService decisionService;
    @Autowired
    private IndicatorDecisionService indicatorDecisionService;

    @Autowired
    private TradingCandlesService tradingCandlesService;

    @Autowired
    private AsyncProcessAwaitFactory awaitFactory;

    public DecisionFitness calculateFitness(CalculateDecisionByIndicatorFitnessRequest request) {
        logger.info("Calculating fitness for " + request.getIndicator()+ " " + request.getSymbol());
        SortedMap<Long, DecisionPredicate> predicates = getPredicates(request);
        return doCalculateFitness(request, predicates);
    }

    public DecisionFitness calculateFitness(CalculateSyntheticFitnessRequest request) {
        logger.info("Calculating synthetic fitness for " + request.getSymbol());
        return doCalculateFitness(request, request.getDecisions());
    }

    private DecisionFitness doCalculateFitness(CalculateFitnessRequest request, SortedMap<Long, DecisionPredicate> predicates) {
        Map<Long, TradingCandle> candles = getCandles(request);
        FitnessCalculator calculator = new FitnessCalculator(request.getPositionTypes());
        Long lastDate = null;
        for (Long date : predicates.keySet()) {
            calculator.decide(predicates.get(date), candles.get(date));
            lastDate = date;
        }
        calculator.markLastPositionAsClosed(candles.get(lastDate));

        return calculator.getFitness();
    }

    private SortedMap<Long, DecisionPredicate> getPredicates(CalculateDecisionByIndicatorFitnessRequest request) {
        SortedMap<Long, DecisionPredicate> predicates = new TreeMap<>(decisionService.findByRequest(request));
        if (predicates.isEmpty()) {
            AsyncProcess process = indicatorDecisionService.decide(request);
            try {
                awaitFactory.createFor(createStatusChecker(process.getProcessId())).get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        predicates = new TreeMap<>(decisionService.findByRequest(request));
        return predicates;
    }

    private Map<Long, TradingCandle> getCandles(DecisionRequest request) {
        return tradingCandlesService.getCandles(request.getSymbol(), request.getPeriod(), request.getFrom(), request.getTo());
    }

    private AsyncProcessStatusChecker createStatusChecker(UUID processId) {
        return () -> indicatorDecisionService.getProcessStatus(processId);
    }
}
