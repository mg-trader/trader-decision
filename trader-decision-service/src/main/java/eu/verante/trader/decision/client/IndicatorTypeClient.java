package eu.verante.trader.decision.client;

import eu.verante.trader.indicator.domain.IndicatorTypeResource;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "indicatorType", url = "${clients.indicatorType.url}")
public interface IndicatorTypeClient extends IndicatorTypeResource {

}
