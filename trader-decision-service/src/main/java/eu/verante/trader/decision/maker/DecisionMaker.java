package eu.verante.trader.decision.maker;

import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.maker.indicator.configuration.DecisionMakerConfiguration;

import java.time.ZonedDateTime;

public interface DecisionMaker<DMC extends DecisionMakerConfiguration> {

    void initialize(DMC config);

    DecisionPredicate make(ZonedDateTime date);
}
