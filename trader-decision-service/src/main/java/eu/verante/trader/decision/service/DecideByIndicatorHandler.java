package eu.verante.trader.decision.service;

import eu.verante.trader.decision.domain.DecideByIndicatorRequest;
import eu.verante.trader.util.asyncprocess.AsyncProcessHandler;

public interface DecideByIndicatorHandler extends AsyncProcessHandler<DecideByIndicatorRequest> {
}
