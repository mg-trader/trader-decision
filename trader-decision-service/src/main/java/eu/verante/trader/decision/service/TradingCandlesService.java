package eu.verante.trader.decision.service;

import eu.verante.trader.domain.candle.TradingCandleQuery;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;

import java.util.Map;

public interface TradingCandlesService {

    Map<Long, TradingCandle> getCandles(String symbol, Period period, Long from, Long to);
}
