package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.decision.domain.DecisionType;
import eu.verante.trader.decision.maker.indicator.configuration.DecisionMakerConfiguration;
import eu.verante.trader.decision.maker.indicator.configuration.EmptyDecisionMakerConfiguration;
import eu.verante.trader.indicator.domain.IndicatorValueType;
import eu.verante.trader.indicator.domain.value.CompareIndicatorValue;

import java.time.ZonedDateTime;

public class CompareDecisionMaker
        extends IndicatorValueDecisionMaker<CompareIndicatorValue, EmptyDecisionMakerConfiguration> {
    protected CompareDecisionMaker(IndicatorValueProvider<CompareIndicatorValue> provider, ObjectMapper mapper) {
        super(provider, mapper);
    }

    @Override
    public void initialize(EmptyDecisionMakerConfiguration config) {}

    @Override
    public DecisionPredicate make(ZonedDateTime date) {
        CompareIndicatorValue indicatorValue = getValueProvider().getValue(date);
        String context = createContext(IndicatorValueType.COMPARE, indicatorValue);

        DecisionPredicate predicate = new DecisionPredicate(DecisionType.HOLD, context);
        if (indicatorValue.isHasCrossed()) {
            predicate = switch (indicatorValue.getComparison()) {
                case HIGHER -> new DecisionPredicate(DecisionType.BUY, context);
                case LOWER -> new DecisionPredicate(DecisionType.SELL, context);
                default -> predicate;
            };
        }
        return predicate;
    }
}
