package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.stock.candle.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisServerCommands;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static eu.verante.trader.decision.maker.indicator.IndicatorHashKey.key;

@Component
public class RedisIndicatorValueCache implements IndicatorValueCache {

    @Autowired
    private RedisTemplate<String, IndicatorHashKey> valueTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public <V extends IndicatorValue> void store(Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to,
                      SortedMap<ZonedDateTime, PreloadedValue<V>> values) {
        HashOperations<String, IndicatorHashKey, Map<Long, String>> hashOps = valueTemplate.opsForHash();
        hashOps.put("INDICATOR_VALUE",
                key(indicator, symbol, period, from, to),
                values.entrySet().stream()
                        .collect(Collectors.toMap(
                                entry -> entry.getKey().toInstant().toEpochMilli(),
                                entry -> toJson(entry.getValue().getValue())
                        )));
    }

    @Override
    public <V extends IndicatorValue> SortedMap<ZonedDateTime, PreloadedValue<V>> load(Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to, IndicatorType type) {
        HashOperations<String, IndicatorHashKey, Map<String, String>> hashOps = valueTemplate.opsForHash();
        Map<String, String> redisValues = hashOps.get("INDICATOR_VALUE", key(indicator, symbol, period, from, to));

        if (redisValues != null) {
            SortedMap<String, String> indicatorValues = new TreeMap<>(redisValues);
            SortedMap<ZonedDateTime, PreloadedValue<V>> preloadedValues = new TreeMap<>();
            Optional<PreloadedValue<V>> last = Optional.empty();
            for (Map.Entry<String, String> entry : indicatorValues.entrySet()) {
                ZonedDateTime key = Instant.ofEpochMilli(Long.valueOf(entry.getKey())).atZone(ZoneId.systemDefault());

                PreloadedValue<V> value = new PreloadedValue<>();
                value.setValue((V)fromJson(entry.getValue(), type.getValueType().getValueClass()));
                value.setPrevious(last);
                last.ifPresent( l-> l.setNext(Optional.of(value)));

                preloadedValues.put(key, value);
                last = Optional.of(value);
            }

            return preloadedValues;
        }
        return null;
    }
    @Override
    public void clean() {
        valueTemplate.getConnectionFactory().getConnection().serverCommands().flushDb(RedisServerCommands.FlushOption.ASYNC);
    }

    private String toJson(IndicatorValue value) {
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private <N extends IndicatorValue> N fromJson(String value, Class<N> expectedType) {
        try {
            return objectMapper.readValue(value, expectedType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
