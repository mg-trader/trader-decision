package eu.verante.trader.decision.service;

import eu.verante.trader.decision.client.TradingCandlesClient;
import eu.verante.trader.domain.candle.TradingCandleQuery;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CachingTradingCandlesClient implements TradingCandlesService {

    @Autowired
    private TradingCandlesClient tradingCandlesClient;
    @Override
    @Cacheable("candlesCache")
    public Map<Long, TradingCandle> getCandles(String symbol, Period period, Long from, Long to) {
        TradingCandleQuery query = TradingCandleQuery.builder()
                .from(from)
                .to(to)
                .build();
        Map<Long, TradingCandle> candles = tradingCandlesClient.getCandles(symbol, period, query)
                .stream()
                .collect(Collectors.toMap(
                        candle -> candle.time().toInstant().toEpochMilli(),
                        candle -> candle
                ));
        return candles;
    }
}
