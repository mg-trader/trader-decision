package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.domain.Indicator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
public class IndicatorValueProviderFactory {
    private final Log logger = LogFactory.getLog(IndicatorValueProviderFactory.class);
    @Autowired
    private BeanFactory beanFactory;

    public IndicatorValueProvider<?> create(Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        PreloadingIndicatorValueProvider provider = beanFactory.getBean(PreloadingIndicatorValueProvider.class);
        logger.info("preloading indicator values for: " + indicator.getName());
        provider.preloadValues(indicator, symbol, period, from, to);
        logger.info("preloading completed for: " + indicator.getName());
        return provider;
    }
}
