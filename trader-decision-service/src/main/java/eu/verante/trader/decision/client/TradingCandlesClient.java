package eu.verante.trader.decision.client;

import eu.verante.trader.domain.candle.TradingCandleResource;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "candles", url = "${clients.candles.url}")
public interface TradingCandlesClient extends TradingCandleResource {

}
