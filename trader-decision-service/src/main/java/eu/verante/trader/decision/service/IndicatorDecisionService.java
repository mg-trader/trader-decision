package eu.verante.trader.decision.service;

import eu.verante.trader.decision.domain.DecideByIndicatorRequest;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import eu.verante.trader.util.asyncprocess.AsyncProcessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class IndicatorDecisionService {

    @Autowired
    private AsyncProcessManager processManager;
    @Autowired
    private DecideByIndicatorHandler decideByIndicatorHandler;


    public AsyncProcess decide(DecideByIndicatorRequest request) {
        return processManager.runProcess(request, decideByIndicatorHandler);
    }

    public AsyncProcess getProcessStatus(UUID processId) {
        return processManager.getById(processId);
    }
}
