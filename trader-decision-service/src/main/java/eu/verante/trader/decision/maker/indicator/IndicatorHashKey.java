package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.domain.Indicator;

import java.time.ZonedDateTime;

public record IndicatorHashKey(
    String indicatorName,
    String symbol,
    Period period,
    long dateMillisFrom,
    long dateMillisTo){
    public static IndicatorHashKey key(Indicator indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        return new IndicatorHashKey(indicator.getName(), symbol, period,
                from.toInstant().toEpochMilli(),
                to.toInstant().toEpochMilli());
    }
}
