package eu.verante.trader.decision.client;

import eu.verante.trader.indicator.domain.IndicatorResource;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "indicator", url = "${clients.indicator.url}")
public interface IndicatorClient extends IndicatorResource {

}
