package eu.verante.trader.decision.maker.indicator;

import eu.verante.trader.decision.maker.indicator.configuration.SimpleOscillatorConfiguration;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CCIDecisionMakerFactory extends AbstractIndicatorValueDecisionMakerFactory
        <SimpleIndicatorValue<Double>, SimpleOscillatorConfiguration, CCIDecisionMaker>{
    protected CCIDecisionMakerFactory() {
        super("CCI");
    }

    @Override
    public CCIDecisionMaker create(IndicatorValueProvider<SimpleIndicatorValue<Double>> provider, Map<String, Object> configuration) {
        CCIDecisionMaker decisionMaker = new CCIDecisionMaker(provider, getObjectMapper());
        decisionMaker.initialize(toConfiguration(configuration, SimpleOscillatorConfiguration.class));
        return decisionMaker;
    }
}
