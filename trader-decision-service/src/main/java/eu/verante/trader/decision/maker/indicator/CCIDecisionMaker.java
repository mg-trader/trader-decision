package eu.verante.trader.decision.maker.indicator;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;

public class CCIDecisionMaker
        extends AbstractOscillatorDecisionMaker{
    protected CCIDecisionMaker(IndicatorValueProvider<SimpleIndicatorValue<Double>> valueProvider, ObjectMapper mapper) {
        super(valueProvider, mapper, 0.0d);
    }
}
