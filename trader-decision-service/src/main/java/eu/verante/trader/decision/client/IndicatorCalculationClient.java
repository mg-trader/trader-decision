package eu.verante.trader.decision.client;

import com.fasterxml.jackson.databind.JsonNode;
import eu.verante.trader.indicator.domain.CalculationResource;
import eu.verante.trader.indicator.domain.request.CalculationRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(name = "indicatorCalculation", url = "${clients.indicatorCalculation.url}")
public interface IndicatorCalculationClient extends CalculationResource {

    @GetMapping("/values")
    Map<Long, JsonNode> getCalculationResultsRaw(@SpringQueryMap CalculationRequest request);
}
