package eu.verante.trader.decision.domain;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Data
public class Transaction {

    private TransactionType type;
    private ZonedDateTime time;
    private double price;

    private double volume;

    private List<String> context;

    public double getValue() {
        return price*volume;
    }
}
