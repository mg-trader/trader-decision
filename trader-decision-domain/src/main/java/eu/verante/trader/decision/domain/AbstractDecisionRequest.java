package eu.verante.trader.decision.domain;

import eu.verante.trader.stock.candle.Period;
import lombok.Data;

@Data
public abstract class AbstractDecisionRequest implements DecisionRequest {

    String symbol;
    Period period;
    Long from;
    Long to;
}
