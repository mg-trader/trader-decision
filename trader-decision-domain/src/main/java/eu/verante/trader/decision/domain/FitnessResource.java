package eu.verante.trader.decision.domain;

import eu.verante.trader.decision.domain.CalculateDecisionByIndicatorFitnessRequest;
import eu.verante.trader.decision.domain.CalculateSyntheticFitnessRequest;
import eu.verante.trader.decision.domain.DecisionFitness;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface FitnessResource {
    @PostMapping("/indicator")
    DecisionFitness calculateFitnessByRequest(@RequestBody CalculateDecisionByIndicatorFitnessRequest request);

    @PostMapping("/synthetic")
    DecisionFitness calculateFitness(@RequestBody CalculateSyntheticFitnessRequest request);
}
