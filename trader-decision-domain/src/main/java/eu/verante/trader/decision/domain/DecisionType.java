package eu.verante.trader.decision.domain;

public enum DecisionType {
    BUY,
    SELL,
    HOLD
}
