package eu.verante.trader.decision.domain;

import eu.verante.trader.decision.domain.DecideByIndicatorRequest;
import eu.verante.trader.decision.domain.DecisionPredicate;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;
import java.util.UUID;

public interface IndicatorDecisionResource {
    //post
    @PostMapping()
    AsyncProcess decide(@RequestBody DecideByIndicatorRequest request);

    @GetMapping("/{processId}/status")
    AsyncProcess getProcessStatus(@PathVariable("processId") UUID processId);

    @PostMapping("/result")
    Map<Long, DecisionPredicate> findByRequest(@RequestBody DecideByIndicatorRequest request);

    @PostMapping("/cleanup")
    void cleanupAllDecisions();
}
