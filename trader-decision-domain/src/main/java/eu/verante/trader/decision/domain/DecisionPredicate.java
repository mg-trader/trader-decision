package eu.verante.trader.decision.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class DecisionPredicate {

    private DecisionType type;
    private List<String> context;

    public DecisionPredicate(DecisionType type, List<String> context) {
        this.type = type;
        this.context = new ArrayList<>(context);
    }

    public DecisionPredicate(DecisionType type, String context) {
        this(type, List.of(context));
    }

    public void addToContext(String context) {
        this.context.add(context);
    }
}
