package eu.verante.trader.decision.domain;

import lombok.Data;

import java.util.Map;

@Data
public class DecideByIndicatorRequest extends AbstractDecisionRequest {

    String indicator;

    Map<String, Object> configuration;
}
