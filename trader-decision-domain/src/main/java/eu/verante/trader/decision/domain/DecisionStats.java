package eu.verante.trader.decision.domain;

import lombok.Data;

@Data
public class DecisionStats {

    long positions;
    long profitablePositions;

    double profitablePositionsPercent;

    double profit;

    double profitPercent;
}
