package eu.verante.trader.decision.domain;

public enum TransactionType {

    OPEN,
    CLOSE
}
