package eu.verante.trader.decision.domain;

import java.util.List;

public interface CalculateFitnessRequest extends DecisionRequest {

    List<DecisionType> getPositionTypes();
}
