package eu.verante.trader.decision.domain;

import lombok.Data;

import java.util.List;

@Data
public class DecisionFitness {

    List<Position> positions;

    Position currentPosition;

    DecisionStats totalStats;

    DecisionStats buyStats;

    DecisionStats sellStats;

}
