package eu.verante.trader.decision.domain;

import lombok.Data;

import java.util.List;

@Data
public class CalculateDecisionByIndicatorFitnessRequest extends DecideByIndicatorRequest
        implements CalculateFitnessRequest{
    private List<DecisionType> positionTypes;
}
