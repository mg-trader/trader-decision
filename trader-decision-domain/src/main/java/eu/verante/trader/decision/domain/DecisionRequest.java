package eu.verante.trader.decision.domain;

import eu.verante.trader.stock.candle.Period;

public interface DecisionRequest {

    String getSymbol();
    Period getPeriod();

    Long getFrom();
    Long getTo();
}
