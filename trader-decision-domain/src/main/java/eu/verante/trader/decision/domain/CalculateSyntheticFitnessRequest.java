package eu.verante.trader.decision.domain;

import lombok.Data;

import java.util.List;
import java.util.SortedMap;

@Data
public class CalculateSyntheticFitnessRequest extends AbstractDecisionRequest
        implements CalculateFitnessRequest{

    private List<DecisionType> positionTypes;
    private SortedMap<Long, DecisionPredicate> decisions;
}
