package eu.verante.trader.decision.domain;

import lombok.Data;

@Data
public class Position {

    private DecisionType positionType;

    private double profit;
    private double profitPercent;

    private Transaction open;

    private Transaction close;
}
