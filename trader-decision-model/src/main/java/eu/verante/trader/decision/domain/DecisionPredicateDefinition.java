package eu.verante.trader.decision.domain;

import lombok.Data;

import java.util.List;

@Data
public class DecisionPredicateDefinition {

    private DecisionTypeDefinition type;
    private List<String> context;
}
