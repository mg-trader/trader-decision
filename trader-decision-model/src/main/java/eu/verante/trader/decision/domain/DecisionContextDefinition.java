package eu.verante.trader.decision.domain;

import lombok.Data;

@Data
public class DecisionContextDefinition {
    private DecisionContextTypeDefinition type;
    private String value;
}
