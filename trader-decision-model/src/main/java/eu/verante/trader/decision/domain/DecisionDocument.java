package eu.verante.trader.decision.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.ZonedDateTime;
import java.util.SortedMap;
import java.util.UUID;

@Document("decision")
@Data
public class DecisionDocument {
    @Id
    private UUID id;

    private Long createdAt;

    private DecisionGroupTypeDefinition decisionGroupTypeDefinition;

    private DecisionMakerInfo decisionMakerInfo;

    private String symbol;

    private String period;

    private SortedMap<Long, DecisionPredicateDefinition> decisions;

    public void asNewDocument() {
        this.id = UUID.randomUUID();
        this.createdAt = ZonedDateTime.now().toInstant().toEpochMilli();
    }
}
