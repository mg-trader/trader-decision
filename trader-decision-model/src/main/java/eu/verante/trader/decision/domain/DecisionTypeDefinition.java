package eu.verante.trader.decision.domain;

public enum DecisionTypeDefinition {
    BUY,
    SELL,
    HOLD
}
