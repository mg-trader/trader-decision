package eu.verante.trader.decision.domain;

import lombok.Data;

import java.util.Map;

@Data
public class IndicatorDecisionMakerInfo implements DecisionMakerInfo {
    private String indicatorName;
    private String indicatorType;

    private Map<String, Object> configuration;
}
