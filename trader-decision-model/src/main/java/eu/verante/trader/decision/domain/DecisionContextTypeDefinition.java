package eu.verante.trader.decision.domain;

public enum DecisionContextTypeDefinition {

    INDICATOR_VALUE_SIMPLE,
    INDICATOR_VALUE_COMPARE,
    INDICATOR_VALUE_TREND;
}
