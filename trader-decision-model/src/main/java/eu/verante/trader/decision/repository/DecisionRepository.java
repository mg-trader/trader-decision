package eu.verante.trader.decision.repository;

import eu.verante.trader.decision.domain.DecisionDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface DecisionRepository extends MongoRepository<DecisionDocument, UUID> {
}
